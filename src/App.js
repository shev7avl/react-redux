import './App.css';
import Register from './components/js/Register';
import Counter from './components/js/Counter';
import Links from './components/js/Links';
import Login from './components/js/Login';
import Layout from './components/js/Layout';
import Home from './components/js/Home';
import Admin from './components/js/Admin';
import Customer from './components/js/Customer';
import NotFound404 from './components/js/NotFound404';
import RequireAuth from './components/js/RequireAuth';
import Unauthorized from './components/js/Unauthorized';
import { Routes, Route, BrowserRouter }  from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AuthProvider } from './context/AuthProvider';

const ROLES = {
  'User': 2001,
  'Customer': 1984,
  'Admin': 1337

}


function App() {
  return (

    <Routes>

      <Route path="/" element={<Layout />}>
        {/*public routes*/}
        <Route path="register" element={<Register />} />
        <Route path="login" element={<Login />} />
        <Route path="/" element={<Links />} />
        <Route path="unauthorized" element={<Unauthorized />} />


        {/*protected routes*/}
        <Route element={<RequireAuth allowedRoles={[ROLES.User]} />}>
          <Route path="home" element={<Home />} />
        </Route>

        <Route element={<RequireAuth allowedRoles={[ROLES.Customer]} />}>
          <Route path="customer" element={<Customer />} />
        </Route>

        <Route element={<RequireAuth allowedRoles={[ROLES.Admin]} />}>
          <Route path="admin" element={<Admin />} />
        </Route>

        {/*catch all */}
        <Route path="*" element={<NotFound404 />} />
      </Route>
    </Routes>    
  );
}

export default App;
