import React from 'react'
import {Redirect, Link} from 'react-router-dom'
import { DO_SOMETHING } from '../../constants/action-types'

const Admin = () => {
  return( 
          <section>
            <h1>Admin Page</h1>
            <br />
            <p>Divide and admin</p>
            <br />
            <Link to="/login">
              <button type="button">
                Log out
              </button>
            </Link>
            <br />
        </section>
  )
}

export default Admin