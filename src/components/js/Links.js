import { useContext } from "react";
import AuthContext from "../../context/AuthProvider";
import { useNavigate, Link } from "react-router-dom";
import "../css/Home.css";
import { Button, Container, Row, Col } from 'react-bootstrap';
import React from 'react'

const Links = () => {
    return (
        <section>


        <Container fluid>
            <h1>Links</h1>
            <br />
            <Row>
<Col>
        <Link to='/counter'>
                    <Button variant="primary" type="button">
                            Redux sample
                        </Button>
        </Link>
</Col>
            </Row>

            <Row>
                <Col>
                    <Link to='/login'>
                        <Button variant="primary" type="button">
                            Login
                        </Button>
                    </Link>
                </Col>
                <Col>
                    <Link to='/register'>
                        <Button variant="secondary" type="button">
                            Register
                        </Button>
                    </Link>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Link to='/unicorn'>
                        <Button variant="success" type="button">
                            Unicorn
                        </Button>
                    </Link>
                </Col>

                <Col>
                    <Link to='/'>
                        <Button variant="warning" type="button">
                            Home
                        </Button>
                    </Link>
                </Col>
            </Row>

            <Row>
                <Col>
                    <Link to='/customer'>
                        <Button variant="danger" type="button">
                            Customer
                        </Button>
                    </Link>
                </Col>

                <Col><Link to='/admin'>
                    <Button variant="info" type="button">
                        Admin
                    </Button>
                </Link></Col>

                <Col><Link to='/unauthorized'>
                    <Button variant="dark" type="button">
                        Unauth
                    </Button>
                </Link></Col>
            </Row>


        </Container>
        </section>
    )
}

export default Links