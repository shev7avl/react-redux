import React from 'react';
import {useNavigate} from 'react-router-dom';

const Unathorized = () => {

    const navigate = useNavigate();
    const goBack = () => navigate(-1);

  return (
    <section>
        <h1>Unauthorized</h1>

        <p>Oops, it seems your access is too small.</p>

        
          <button type="button" onClick={goBack}>Go Back</button>
            

        </section>
  )
}

export default Unathorized