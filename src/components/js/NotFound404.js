import React from 'react';
import {Link} from 'react-router-dom';

const NotFound404 = () => {
  return (
    <section>
        <h1>Oops, nothing found</h1>

        <h1>404</h1>

        <h3>We're looking for your page</h3>

        <Link to="/">
          <button type="button">
            Go home
          </button>
        </Link>
        </section>
  )
}

export default NotFound404