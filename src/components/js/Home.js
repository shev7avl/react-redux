import { useContext } from "react";
import AuthContext from "../../context/AuthProvider";
import {useNavigate, Link} from "react-router-dom";
import "../css/Home.css";

import React from 'react'

const Home = () => {
  return (
    <section>
            <h1>Home</h1>
            <br />
            <p>Welcome home, good hunter</p>
            <br />
            <Link to="/login">
              <button type="button">
                Log out
              </button>
            </Link>
            <br />
            <Link to="/unicorn">
              <button type="button">
                Find a unicorn
              </button>
            </Link>
            <br />               

        </section>
  )
}

export default Home