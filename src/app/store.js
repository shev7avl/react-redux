import {configureStore} from '@reduxjs/toolkit'
import counterReducer from './api/counterSlice'

export const store = configureStore(
    {reducer: {
        counter: counterReducer,
        
    },    
    
}
)