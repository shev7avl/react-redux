import { buildError } from "terser-webpack-plugin"
import {apiSlice} from "../../app/api/apiSlice"

export const authApiSlice = apiSlice.injectEndpoints({
    endpoints: builder => ({
        login: builder.mutation(
            {
                query: credentials => (
                    {
                        url: '/auth',
                        method: 'POST',
                        body: {...credentials}
                    }
                )
            }
        )

    })
})